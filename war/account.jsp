<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>en-co.de</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="en-co.de">
    <meta name="author" content="Brandt Westing">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="img/favicon.ico">
    
    <script type="text/javascript" src="js/soyutils.js"></script>
    <script type="text/javascript" src="js/navbar.js"></script>
    <script type="text/javascript" src="js/navbar_util.js"></script>
  </head>

  <body>
    <script type="text/javascript">
    // Exercise the .helloWorld template
        document.write(encode.navbar.navbar());
    </script>

    <div class="container">
        <h2>Account Settings</h2>
        <ul class="nav nav-list">
            <li class="divider"></li>
        </ul>
        <div class="row">
            <div class="span3">
		        <p><small>The information provided here can be included at your discretion in any of your <strong>en-co.de</strong> tags.</small></p>
		        
		        <p><small>By default, <strong>en-co.de</strong> does not share your personal information in QR tag pages.
		        You may wish to adjust these settings in the <em>shared information</em> section so that others may contact you
		        when a tag is scanned. </small></p>
		        
		        <p><small>Not sharing information has no effect on whether you receive updates when a tag is scanned.</small></p>
		        
		        <p><small>Your authorization token is used for the en-co.de API. <br><br>Token: <code id="token"></code>.</small></p>
		    </div>
		    
		    <div class="span6 offset1">
		       <form action="/updateAccount" method="post" id="inputForm">
		          <div class="row">
				    <h3>Identification</h3>
				    <small>Identification Information is stored internally at <em>en-co.de</em> and only shared at your discretion.</small>
		            <ul class="nav nav-list">
		                <li class="divider"></li>
		            </ul>
		              <div class="span3">
						  <div class="control-group">
						    <label class="control-label" for="inputForm-name">Full Name</label>
						    <div class="controls">
						      <input type="text" name="name" id="inputForm-name" placeholder="Full Name">
						    </div>
						  </div>
						  <div class="control-group">
						    <label class="control-label" for="inputForm-email">Email Address</label>
						    <div class="controls">
						      <input type="text" name="email" id="inputForm-email" placeholder="brandt@en-co.de">
						    </div>
						  </div>
						  <div class="control-group">
	                        <label class="control-label" for="inputForm-phone">Phone Number</label>
	                        <div class="controls">
	                          <input type="text" name="phone" id="inputForm-phone" placeholder="555-555-5555">
	                        </div>
	                      </div>
                      </div>
                      <div class="span3">
                          <div class="control-group">
                            <label class="control-label" for="inputForm-addStreet">Street Address</label>
                            <div class="controls">
                              <input type="text" name="addStreet" id="inputForm-addStreet" placeholder="123 Simplify Lane Apt 20">
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label" for="inputForm-addCity">City</label>
                            <div class="controls">
                              <input type="text" name="addCity" id="inputForm-addCity" placeholder="Austin">
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label" for="inputForm-addState">State</label>
                            <div class="controls">
                              <input type="text" name="addState" id="inputForm-addState" placeholder="TX">
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label" for="inputForm-add">Zip Code</label>
                            <div class="controls">
                              <input type="text" name="addZip" id="inputForm-addZip" placeholder="78702">
                            </div>
                          </div>
                        </div>
                        </div>
					<div class="row">
			            <h3>Shared Information</h3>
			            <small>Information selected here will be shown on your tag pages by anyone who scans a tag.</small>
			            <ul class="nav nav-list">
			                <li class="divider"></li>
			            </ul>
			            <label class="checkbox">
					      <input type="checkbox" name="allowName" id="inputForm-allowName" checked/> Allow others to see my name when scanning tags. 
					    </label>
					    <label class="checkbox">
                          <input type="checkbox" name="allowEmail" id="inputForm-allowEmail"/> Allow others to see my email when scanning tags. 
                        </label>
                        <label class="checkbox">
                          <input type="checkbox" name="allowPhone" id="inputForm-allowPhone"/> Allow others to see my phone number when scanning tags. 
                        </label>
                        <label class="checkbox">
                          <input type="checkbox" name="allowAdd" id="inputForm-allowAdd"/> Allow others to see my address when scanning tags. 
                        </label>
		            </div>
		            <div class="row">
                        <h3>Save Changes</h3>
                        <small>Saving changes may allow others to see shared information when scanning your tags.</small>
                        <ul class="nav nav-list">
                            <li class="divider"></li>
                        </ul>
                        <button type="submit" class="btn" id="inputForm-submit">Save Changes</button>
                        <input type="hidden" name="username" id="inputForm-username"/>
                        <span class="help-inline"><p id="submit-notifier"></p></span>
                    </div>
	            </form>
		    </div>
		</div>

		<hr>
		
		<footer>
		    <p>&copy; en-co.de 2013</p>
		</footer>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.9.1.min.js"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script>
    <script src="js/jquery.cookie.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/md5.js"></script>
    <script src="js/login.js"></script>
    
    <script>
    $(document).ready(function() {
    	encode.navbar_util.initialize();
    	
    	$('#token').text(localStorage['authenticationToken']);
    	
    	// populate form with data from server
    	var username = localStorage['username'];
    	$.getJSON('/accountInfo', {username: username}, function(json){
    		$('#inputForm-name').val(json.name);
    		$('#inputForm-email').val(json.email);
    		$('#inputForm-phone').val(json.phone);
    		$('#inputForm-addStreet').val(json.addStreet);
    		$('#inputForm-addCity').val(json.addCity);
    		$('#inputForm-addState').val(json.addState);
    		$('#inputForm-addZip').val(json.addZip);
    		
    	    if(json.allowName)
                $('#inputForm-allowName').prop('checked', true);
            else
        	    $('#inputForm-allowName').prop('checked', false);
    	    if(json.allowEmail)
                $('#inputForm-allowEmail').prop('checked', true);
            else
                $('#inputForm-allowEmail').prop('checked', false);
    	    if(json.allowPhone)
                $('#inputForm-allowPhone').prop('checked', true);
            else
                $('#inputForm-allowPhone').prop('checked', false);
    	    if(json.allowAdd)
                $('#inputForm-allowAdd').prop('checked', true);
            else
                $('#inputForm-allowAdd').prop('checked', false);
        });
    	
    	// submits the account update to the server
        $('#inputForm').submit(function() {
        	$('#inputForm-username').val(localStorage['username']);
        	
        	// set up form controls
            var options = { 
                success:       showResponse,  // post-submit callback 
                dataType:  'json'        // 'xml', 'script', or 'json' (expected server response type)
            };
        	
            $(this).ajaxSubmit(options);
            
            return false;
        });
    	
    	
    });
    
    // post-submit callback 
    function showResponse(responseText, statusText, xhr, $form)  { 
        $.each(responseText, function (index, value) {
            if(value == "fail"){
                $('#submit-notifier').text("Error saving information.");
                $('#submit-notifier').removeClass("text-success");
                $('#submit-notifier').addClass("text-error");
                $('#submit-notifier').fadeIn().delay(1500).fadeOut(1500);
            }
            else {
                $('#submit-notifier').text("Your information successfully saved.");
                $('#submit-notifier').removeClass("text-error");
                $('#submit-notifier').addClass("text-success");
                $('#submit-notifier').fadeIn().delay(1500).fadeOut(1500);
                
            }
        });
    };
    </script>

  </body>
</html>