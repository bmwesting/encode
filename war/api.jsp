<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>en-co.de</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="en-co.de">
    <meta name="author" content="Brandt Westing">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="img/favicon.ico">
    
    <script type="text/javascript" src="js/soyutils.js"></script>
    <script type="text/javascript" src="js/navbar.js"></script>
    <script type="text/javascript" src="js/navbar_util.js"></script>
  </head>

  <body>
    <script type="text/javascript">
        document.write(encode.navbar.navbar());
    </script>

    <div class="container">
    
        <h2>API</h2>
        <p>
        The en-co.de application programming interface (API) can be used to query or add to the en-co.de tag database.
        API requests are responded to with <a href="http://www.json.org/">Json</a>-coded responses.
        </p>
        
        <p>
        Some API functions require the user to supply an authentication token <em>{token}</em>. The authentication token can be found in <a href="/account.jsp">Account Settings</a>.
        </p>
        
        <h3>Querying Tag Data</h3>
        You can query for the data associated with a tag ID: <code>HTTP GET en-co.de/retrieveTag?key={tagID}</code>.<br><br>

        <p>The HTTP request responds with information related to the specified <strong>tagID</strong>:</p>
        
        <pre>
        { 
          "name":"My Dog",
          "description":"Doggy",
          "ownerName":"Joe Simple",
          "email":"email@en-co.de",
          "phone":"555-555-5555",
          "addStreet":"123 Any Street",
          "addCity":"Austin",
          "addState":"TX",
          "addZip":"78704",
          "messaging":true,
          "tracking":true
        }
        </pre>
        
        <h3>Updating Tag Data</h3>
        You can update the data associated with a tag ID: <code>HTTP GET en-co.de/editTag?key={ID}&auth={token}&name=NewNameString&description=NewDescriptionString</code>.<br><br>

        <p>The HTTP request updates the tag metadata and return "ok" of "auth_error" status.</p>
        
        <h3>Querying for All Tags by Username</h3>
        You can query for the data associated with a username: <code>HTTP GET en-co.de/retrieveTags?user={username}&auth={token}</code>.<br><br>

        <p>The HTTP request responds with all the tags belonging to the user:</p>
        
        <pre>
        [{ 
          "name":"My Dog",
          "description":"Doggy",
          "ownerName":"Joe Simple",
          "email":"email@en-co.de",
          "phone":"555-555-5555",
          "addStreet":"123 Any Street",
          "addCity":"Austin",
          "addState":"TX",
          "addZip":"78704",
          "messaging":true,
          "tracking":true
        },
        { 
          "name":"My Book",
          "description":"Count of Monte Cristo",
          "ownerName":"Joe Simple",
          "email":"email@en-co.de",
          "phone":"555-555-5555",
          "addStreet":"123 Any Street",
          "addCity":"Austin",
          "addState":"TX",
          "addZip":"78704",
          "messaging":true,
          "tracking":true
        }]
        </pre>

      <hr>

      <footer>
        <p>&copy; en-co.de 2013</p>
      </footer>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.9.1.min.js"></script>
    <script src="js/jquery.cookie.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/md5.js"></script>
    <script src="js/login.js"></script>
    
    <script>
    $(document).ready(function() {
        encode.navbar_util.initialize();
    });
    </script>

  </body>
</html>