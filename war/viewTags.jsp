<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>en-co.de</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="en-co.de">
    <meta name="author" content="Brandt Westing">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="img/favicon.ico">
    
    <script type="text/javascript" src="js/soyutils.js"></script>
  	<script type="text/javascript" src="js/navbar.js"></script>
  	<script type="text/javascript" src="js/navbar_util.js"></script>
  </head>

  <body>
	<script type="text/javascript">
    // Exercise the .helloWorld template
    document.write(encode.navbar.navbar());
  	</script>

    <div class="container">

      <h2>View Tags</h2>
        <ul class="nav nav-list">
            <li class="divider"></li>
        </ul>
        <div class="row">
            <div class="span3" id="descriptionPane">
                <div class="row">
	                <div style="text-align: center;" id="qrImage-div">
	                    
	                </div>
                </div>
                <div class="row">
	                <ul class="nav nav-list">
	                    <li class="divider"></li>
	                </ul>
                </div>
                <div class="row" style="text-align: center;">
                    <button class="btn btn-primary" id="buttonEdit">Edit</button>
                    <button class="btn btn-danger" id="buttonRemove">Remove</button>
                    <button class="btn btn-primary" id="buttonSave" style="display: none">Save</button>
                    <button class="btn btn-danger" id="buttonCancel" style="display: none">Cancel</button>
                    <button class="btn btn-danger" id="buttonMapCancel" style="display: none">Cancel</button>
                </div>
                <dl>
				    <dt >Name</dt>
				    <dd id="desc-name"></dd>
				    <dt>Description</dt>
                    <dd id="desc-desc"></dd>
                    <dt >Creation Date</dt>
                    <dd id="desc-cdate"></dd>
                    <dt >Last Scanned</dt>
                    <dd id="desc-tscanned"></dd>
                    <dt >Location</dt>
                    <dd id="desc-loc"></dd>
                    <dt>Text Alerts</dt>
                    <dd id="desc-alerts"></dd>
                    <dt>Location Tracking</dt>
                    <dd id="desc-tracking"></dd>
                    <dt>Unique ID</dt>
                    <dd id="desc-key"></dd>
				</dl>
            </div>
            <div class="span6">
	            <table class="table table-hover" id="tagTable">
	               <thead>
	                   <tr>
	                       <th>#</th>
	                       <th id='tagTable-header-name'>Name</th>
	                       <th id='tagTable-header-created'>Date Created</th>
	                       <th id='tagTable-header-scanned'>Last Scanned</th>
	                       <th>Last Location</th>
	                       <th>Unique ID</th>
	                   </tr>
	               </thead>
	               <tbody>
	               </tbody>
	            </table>
	            
	            <!-- Hidden edit form. Shown when edit button pressed. -->
	            <form id="editTagForm" action="/editTag" method="get" style="display: none">
                    <fieldset>
                    <legend>Edit Tag</legend>
                    <label>Name</label>
                    <input type="text" name="name" class="input-xlarge" id="editTagForm-name" placeholder="Name"/>
                    <textarea name="description" class="input-xlarge" rows="3" id="editTagForm-desc" placeholder="Description here..."></textarea>
                    <label class="checkbox">
                    <input type="checkbox" name="tracking" id="editTagForm-tracking" checked/> Location Tracking
                    </label>
                    <label class="checkbox">
                    <input type="checkbox" name="messaging" id="editTagForm-messaging" checked/> Text Message Updates
                    </label>
                    <input type="hidden" name="key" id="editTagForm-key"/>
                    <input type="hidden" name="auth" id="editTagForm-auth"/>
                    <input type="hidden" name="ownerID" id="editTagForm-owner"/>
                    </fieldset>
                </form>
                
                <div id="mapView" style="display: none">
                    
                </div>
	            
            </div>
            <div class="span9" id='noTagsText' style="display:none;">
                <p class="lead">No tags found.</p>
            </div>
        </div>

      <hr>

      <footer>
        <p>&copy; en-co.de 2013</p>
      </footer>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.9.1.min.js"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script>
    <script src="js/jquery.cookie.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/md5.js"></script>
    <script src="js/login.js"></script>
    
    <script>
    
    var json = null;
    var sortName = false;
    var sortCreated = false;
    var sortScanned = false;
    
    $(document).ready(function() {
    	encode.navbar_util.initialize();
    	    	
    	// populate table
        retrieveAndSort();
    	
    	// clicking on table column header sorts by that header
    	$('#tagTable-header-name').click(function(){
    		sortName = !sortName;
    		sortJson('name', sortName);
            buildTable();
    	});
    	
    	// clicking on table column header sorts by that header
        $('#tagTable-header-created').click(function(){
            sortCreated = !sortCreated;
            sortJson('creationDate', sortCreated);
            buildTable();
        });
    	
        // clicking on table column header sorts by that header
        $('#tagTable-header-scanned').click(function(){
            sortScanned = !sortScanned;
            sortJson('tScanned', sortScanned);
            buildTable();
        });
        
        // clicking remove button deletes tag from database and hides from table
        $('#buttonRemove').click(function(){
        	var key = $('#desc-key').text();
        	
        	// request removal from server
        	$.ajax({
              url: '/removeTag',
              data: {key: key},
              success: function(response) {
                  
              }
            });
        	
        	$.each(json, function (index, value) {
                if($.trim(value.key) === $.trim(key)){
                	json.splice(index, 1); // removes from json
                	return false;
                }
        	});
        	
        	// rebuild table - later replace with removal of element only
        	buildTable();
        });
        
        $('#editTagForm').submit(function() {
        	var options = { 
        	        type: 'get', // 'get' or 'post', override for form's 'method' attribute
        	        success:       showResponse
        	    }; 
            $(this).ajaxSubmit(options);
            return false;
        });
        
        // post-submit callback 
        function showResponse(responseText, statusText, xhr, $form)  { 
        }
        
        $('#buttonSave').click(function() {
            // submit the updates
            $('#editTagForm').submit();
                        
            // repopulate table
            retrieveAndSort();
            showView();
        });
        
        $('#buttonCancel').click(function() {
        	showView();
        });
                
        $('#buttonEdit').click(function(){
        	showEdit();
        });
        
        $('#buttonMapCancel').click(function(){
            showViewFromMap();
        });
    	
    });
    
    function showEdit() {
    	$('#tagTable').hide();
        $('#buttonRemove').hide();
        $('#buttonEdit').hide();
        $('#buttonSave').fadeIn();
        $('#buttonCancel').fadeIn();
        
        // update form fields with current data
        $('#editTagForm-name').val($('#desc-name').text());
        $('#editTagForm-desc').text($('#desc-desc').text());
        if($('#desc-messaging').text() == 'Disabled')
            $('#editTagForm-messaging').set(false);
        if($('#desc-tracking').text() == 'Disabled')
            $('#editTagForm-tracking').set(false);
        $('#editTagForm-key').val($('#desc-key').text());
        $('#editTagForm-auth').val(localStorage['authenticationToken']);

        $('#editTagForm').fadeIn();
    }
    
    function showView() {
    	$('#editTagForm').hide();
        $('#buttonSave').hide();
        $('#buttonCancel').hide();
        $('#buttonRemove').fadeIn();
        $('#buttonEdit').fadeIn();
        $('#tagTable').fadeIn();
    }
    
    function showMap() {
    	$('#tagTable').hide();
        $('#buttonRemove').hide();
        $('#buttonEdit').hide();
        $('#buttonMapCancel').fadeIn();
        $('#mapView').fadeIn();
    }
    
    function showViewFromMap() {
    	$('#buttonMapCancel').hide();
    	$('#mapView').hide();
    	$('#buttonRemove').fadeIn();
        $('#buttonEdit').fadeIn();
        $('#tagTable').fadeIn();
    }
    
    // retrieves qr metadata and sorts atomically for initial page load
    function retrieveAndSort(){
    	var username = localStorage['username'];
    	var authentication = localStorage['authenticationToken'];
    	
    	// retrieve json metadata from server
        $.getJSON('/retrieveTags', {user: username, auth: authentication}, function(json_retr){
             json = json_retr;
             
             // no tags in database for user
             if(null == json){
            	 $('#tagTable').hide();
            	 $('#descriptionPane').hide();
            	 $('#noTagsText').show();
             }
             else {
            	 sortJson('creationDate', false);
                 buildTable();
             }
             
        });
    }
    
    function buildTable(){
    	// empty the table
        $('#tagTable > tbody').empty();
    	
    	// each element is a qr tag
        $.each(json, function (index, value) {
            // update table body
            var creationTime = new Date(0);
            var scannedTime  = new Date(0);
            creationTime.setUTCMilliseconds(value.creationDate);
            scannedTime.setUTCMilliseconds(value.tScanned);
            var newRow = "<tr><td>" + parseInt(index+1) + "</td>";
            newRow = newRow + "<td>" + value.name + "</td>";
            newRow = newRow + "<td>" + creationTime.toLocaleDateString() + "</td>";
            if(scannedTime <= new Date(0))
            	newRow = newRow + "<td>" + " N/A " + "</td>";
            else
                newRow = newRow + "<td>" + scannedTime.toLocaleDateString() + "</td>";
            if(value.lat != null)
            	newRow = newRow + "<td>" + value.lat.substring(0,5) + ", " + value.lon.substring(0,5) + "</td>";
            else
            	newRow = newRow + "<td>" + " N/A " + "</td>";	
            newRow = newRow + "<td>" + value.key + "</td></tr>";
            $('#tagTable > tbody:last').append(newRow);
        });
        
    	// set hover callback for table rows
        $('#tagTable > tbody > tr').hover(function() {
        	var key = $(this).find('td:eq(5)').text().trim();
        	hoverAction(key);
        });
    	
    	//set click callback on table rows to open map view when clicked
    	$('#tagTable > tbody > tr').click(function() {
    		var qr = getQRJson($(this).find('td:eq(5)').text());
    		
    		// hide table and show map
    		if(qr.lat != null) {
    			
    			var latlon=qr.lat+","+qr.lon;
    			
    			var img_url="http://maps.googleapis.com/maps/api/staticmap?center="
    			+latlon+"&zoom=15&size=500x400&sensor=false&markers=color:blue%7Clabel:A%7C"+latlon;
    			
    			var html = "<img src=\"" + img_url + "\"/>";
    			
    			$('#mapView').html(html);
    			
    			showMap();
    		}
    	});
    	
    	// first element is by default selected
    	hoverAction($('#tagTable > tbody').find('tr:eq(0)').find('td:eq(5)').text().trim());
    }
    
    function hoverAction(key) {
        var googleQRURL = "https://chart.googleapis.com/chart?cht=qr&chs=140x140&chld=M|0&chl=www.en-co.de/?t=" + 
        key;
        $('#qrImage-div').html("<img src=\"" + googleQRURL + "\" id=\"qrImage\"\/>");
        
        // update description
        $.each(json, function (index, value) {
            if($.trim(value.key) === $.trim(key)){
                $('#desc-name').text(value.name);
                $('#desc-desc').text(value.description);
                
                var creationTime = new Date(0);
                creationTime.setUTCMilliseconds(value.creationDate);
                $('#desc-cdate').text(creationTime.toLocaleString());
                
                if(value.tScanned == '0')
                    $('#desc-tscanned').text('N/A');
                else {
                    var scannedTime  = new Date(0);
                    scannedTime.setUTCMilliseconds(value.tScanned);
                    $('#desc-tscanned').text(scannedTime.toLocaleString());
                }
                
                if(value.lat != null)
                	$('#desc-loc').html(value.lat+", "+value.lon);
                else
                	$('#desc-loc').html("N/A");
                
                if(value.messaging)
                      $('#desc-alerts').text('Enabled');
                else
                    $('#desc-alerts').text('Disabled');
                if(value.tracking)
                      $('#desc-tracking').text('Enabled');
                else
                    $('#desc-tracking').text('Disabled');
                $('#desc-key').text(value.key);
                return false;
            }
        });
    }
    
    function getQRJson(key) {
    	var result;
        $.each(json, function (index, value) {
            if($.trim(value.key) === $.trim(key)){
            	return result = value;
            }
        });
        
        return result;
    }
    
    function sortJson(prop, asc) {
        json = json.sort(function(a, b) {
            if (asc) return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
            else return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
        });
        //showResults();
    }
    </script>

  </body>
</html>