<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>en-co.de</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="en-co.de">
    <meta name="author" content="Brandt Westing">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="img/favicon.ico">
    
    <script type="text/javascript" src="js/soyutils.js"></script>
    <script type="text/javascript" src="js/navbar.js"></script>
    <script type="text/javascript" src="js/navbar_util.js"></script>
  </head>

  <body>
    <script type="text/javascript">
        document.write(encode.navbar.navbar());
    </script>

    <div class="container">
	    <p id="greetingText">
	    </p>
	    
	    <dl class="dl-horizontal" id="desc-table">
		</dl>
		
		<form id="newTagForm" action="/validateTag" method="get" style="display: none">
           <fieldset>
           <legend>New Tag Form</legend>
           <label>Name of Item</label>
           <input type="text" name="name" class="input-xlarge" id="focusedInput" placeholder="Name">
           <textarea name="description" class="input-xlarge" rows="3" placeholder="Description here..."></textarea>
           <label class="checkbox">
           <input type="checkbox" name="tracking" checked> Location Tracking
           </label>
           <label class="checkbox">
           <input type="checkbox" name="messaging" checked> Text Message Updates
           </label>
           <input type="hidden" name="key" id="newTagForm-Key"/>
           <button type="submit" class="btn" id="newTagForm-submit">Validate Tag</button>
           <span class="help-inline"><p id="submit-notifier"></p></span>
           </fieldset>
       </form>

      <hr>

      <footer>
        <p>&copy; en-co.de 2013</p>
      </footer>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.9.1.min.js"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script>
    <script src="js/jquery.cookie.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/md5.js"></script>
    <script src="js/login.js"></script>
    
    <script>
    $(document).ready(function() {
        encode.navbar_util.initialize();
        
        // returns the value of a specified parameter
        function getParameterByName(name) {
            var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
            return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        }
        
        var tagID = getParameterByName('t');
        
        $('#newTagForm-Key').val(tagID);
        
        // submits the newTagFrom to the server
        $('#newTagForm').submit(function() {
        	
            var form_options = { 
                    target:        '#formOutput',   // target element(s) to be updated with server response 
                    success:       showResponse,  // post-submit callback 
                    // other available options: 
                    //url:       url         // override for form's 'action' attribute 
                    //type:      type        // 'get' or 'post', override for form's 'method' attribute 
                    dataType:  'json'        // 'xml', 'script', or 'json' (expected server response type) 
                    //clearForm: true        // clear all form fields after successful submit 
                    //resetForm: true        // reset the form after successful submit 
             
                    // $.ajax options can be used here too, for example: 
                    //timeout:   3000 
            };
            
        	$(this).ajaxSubmit(form_options);
        	
        	return false;
        });
            
        
        // make call to server for tag information
        $.getJSON('/scanTag', {key: tagID}, function(json){
        	
        	// prompt for input
        	if(json.status == "invalid") {
        		$('#desc-table').hide();
        		$('#newTagForm').show();
        	}
        	else {
        		// perform location prompt
                if(json.tracking) {
                    if (navigator.geolocation)
                           navigator.geolocation.getCurrentPosition(function(pos) {
                               // send position to server
                               $.getJSON('/updateLocation', {key: tagID, lat: pos.coords.latitude, lon: pos.coords.longitude, alt: pos.coords.altitude}, function(json) {
                                   return false;
                               });
                           });
                }
        		
                // populate information from json
                $('#desc-table').append("<dt>Name</dt><dd>"+json.name+"</dd>");
                $('#desc-table').append("<dt>Description</dt><dd>"+json.description+"</dd>");
                if(json.ownerName != null)
                       $('#desc-table').append("<dt>Owner Name</dt><dd>"+json.ownerName+"</dd>");
                if(json.email != null)
                       $('#desc-table').append("<dt>Owner Email</dt><dd>"+json.email+"</dd>");
                if(json.phone != null)
                       $('#desc-table').append("<dt>Owner Phone</dt><dd>"+json.phone+"</dd>");
                if((json.addStreet != null) && (json.ownerName != null))
                       $('#desc-table').append("<dt>Owner Address</dt><dd><address>"+json.ownerName+"<br>"+json.addStreet+"<br>"+json.addCity+", "+json.addState+" "+json.addZip+"</address></dd>");
        	}
        	
        });
    });
    
    // post-submit callback 
    function showResponse(responseText, statusText, xhr, $form)  { 
        $.each(responseText, function (index, value) {
            if(value == "fail"){
                $('#submit-notifier').text("Error adding tag to your tag database");
                $('#submit-notifier').removeClass("text-success");
                $('#submit-notifier').addClass("text-error");
                $('#submit-notifier').fadeIn().delay(1500).fadeOut(1500);
            }
            else {
                $('#submit-notifier').text(value + " added to your tag database.");
                $('#submit-notifier').removeClass("text-error");
                $('#submit-notifier').addClass("text-success");
                $('#submit-notifier').fadeIn().delay(1500).fadeOut(1500);
                
            }
        });
    } 
    

    </script>

  </body>
</html>