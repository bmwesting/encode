<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>en-co.de</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="en-co.de">
    <meta name="author" content="Brandt Westing">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="img/favicon.ico">
    
    <script type="text/javascript" src="js/soyutils.js"></script>
  	<script type="text/javascript" src="js/navbar.js"></script>
    <script type="text/javascript" src="js/navbar_util.js"></script>
  </head>

  <body>
	<script type="text/javascript">
	
        // returns the value of a specified parameter
	    function getParameterByName(name) {
	        var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
	        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
	    }
	    	
        // determine if redirect necessary due to tag being scanned
        var tagValue = getParameterByName('t');
        if(null != tagValue)
            window.location.replace("/tag.jsp?t=" + tagValue);
        
        document.write(encode.navbar.navbar());
  	</script>

    <div class="container">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <h1>Unlimited reliable tracking.</h1>
        
        <p>Give a digital presence to your real world items, possessions, or pets.</p>
      </div>

      <!-- Example row of columns -->
      <div class="row">
        <div class="span4">
          <h2>Usability</h2>
          <p>Anybody can use en-co.de! This easy to use service specializes in tracking your possessions. Build a digital collection of you possessions in minutes and print tags using our easy-to-use print templates.</p>
        </div>
        <div class="span4">
          <h2>Tracking</h2>
          <p>En-co.de can accurately track your items and provide real-time updates to you. Whenever someone scans your tagged item, its location is automatically updated for you.</p>
       </div>
        <div class="span4">
          <h2>Text Updates</h2>
          <p>Lost a prized possession or pet? En-co.de alerts you immediately via text message or email if one of your tags is scanned.</p>
        </div>
      </div>

      <hr>

      <footer>
        <p>&copy; en-co.de 2013</p>
      </footer>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.9.1.min.js"></script>
    <script src="js/jquery.cookie.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/md5.js"></script>
    <script src="js/login.js"></script>
    
    <script>
    $(document).ready(function() {
        encode.navbar_util.initialize();
    });
    </script>

  </body>
</html>