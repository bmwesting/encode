<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>en-co.de</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="en-co.de">
    <meta name="author" content="Brandt Westing">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="img/favicon.ico">
    
    <script type="text/javascript" src="js/soyutils.js"></script>
  	<script type="text/javascript" src="js/navbar.js"></script>
	<script type="text/javascript" src="js/navbar_util.js"></script>
  </head>

  <body>
	<script type="text/javascript">
    // Exercise the .helloWorld template
    document.write(encode.navbar.navbar());
    
    // set up form controls
    var form_options = { 
        target:        '#formOutput',   // target element(s) to be updated with server response 
        success:       showResponse,  // post-submit callback 
        // other available options: 
        //url:       url         // override for form's 'action' attribute 
        //type:      type        // 'get' or 'post', override for form's 'method' attribute 
        dataType:  'json'        // 'xml', 'script', or 'json' (expected server response type) 
        //clearForm: true        // clear all form fields after successful submit 
        //resetForm: true        // reset the form after successful submit 
 
        // $.ajax options can be used here too, for example: 
        //timeout:   3000 
    };
    
    // post-submit callback 
    function showResponse(responseText, statusText, xhr, $form)  { 
    	$.each(responseText, function (index, value) {
            if(value == "fail"){
            	$('#submit-notifier').text("Error adding tag to your tag database");
            	$('#submit-notifier').removeClass("text-success");
                $('#submit-notifier').addClass("text-error");
                $('#submit-notifier').fadeIn().delay(1500).fadeOut(1500);
            }
            else {
            	$('#submit-notifier').text(value + " added to your tag database.");
            	$('#submit-notifier').removeClass("text-error");
            	$('#submit-notifier').addClass("text-success");
            	$('#submit-notifier').fadeIn().delay(1500).fadeOut(1500);
            	
            }
    	});
    } 
  	</script>

    <div class="container">
        <h2>Create New Tag</h2>
        <ul class="nav nav-list">
            <li class="divider"></li>
        </ul>
        <div class="row">
            <div class="span3">
                <div style="text-align: center;" id="qrImage-div">
                    
                </div>
                <ul class="nav nav-list">
                    <li class="divider"></li>
                </ul>
                <p><strong>Note:</strong> Creating a new tag will permanently save information in the <a href="/">en-co.de</a> database for archiving. 
                You can later delete or update tags on the <a href="/viewTags.jsp">view tags page</a>.
                </p>
            </div>
            <div class="span6">
                <form id="newTagForm" action="/newTag" method="get">
                    <fieldset>
                    <legend>New Tag Form</legend>
                    <label>Name of Item</label>
                    <input type="text" name="name" class="input-xlarge" id="focusedInput" placeholder="Name">
                    <textarea name="description" class="input-xlarge" rows="3" placeholder="Description here..."></textarea>
                    <label class="checkbox">
                    <input type="checkbox" name="tracking" checked> Location Tracking
                    </label>
                    <label class="checkbox">
                    <input type="checkbox" name="messaging" checked> Text Message Updates
                    </label>
                    <input type="hidden" name="key" id="newTagForm-Key"/>
                    <input type="hidden" name="ownerID" id="newTagForm-Owner"/>
                    <button type="submit" class="btn" id="newTagForm-submit">Submit</button>
                    <span class="help-inline"><p id="submit-notifier"></p></span>
                    </fieldset>
                </form>
            </div>
        </div>

      <hr>

      <footer>
        <p>&copy; en-co.de 2013</p>
      </footer>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.9.1.min.js"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script>
    <script src="js/jquery.cookie.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/md5.js"></script>
    <script src="js/login.js"></script>
    
    <script>
    $(document).ready(function() {
    	encode.navbar_util.initialize();
    	
    	// hide the submit notifier text
    	$('#submit-notifier').hide();
    	$("#qrImage-div").hide();
    	
    	loadRandomQR();
    	
    	// submits the newTagFrom to the server
    	$('#newTagForm').submit(function() {
    		$(this).ajaxSubmit(form_options);
    		$('#qrImage-div').hide();
    		loadRandomQR();
    	    return false;
        });
    });
    
    function loadRandomQR() {
    	// populate QR code and hidden elements
        $.ajax({
              url: 'uuid',
              success: function(response) {
                  //$("#randomField").val(response);
                  $('#newTagForm-Owner').val(localStorage['username']);
                  $('#newTagForm-Key').val(response);
                  var googleQRURL = "https://chart.googleapis.com/chart?cht=qr&chs=140x140&chld=M|0&chl=www.en-co.de/?t=" + 
                    response.replace(/\s/g, '');
                  $('#qrImage-div').html("<img src=\"" + googleQRURL + "\" id=\"qrImage\"\/>");
                  $('#qrImage-div').fadeIn(1000);
              }
            });
    } 
    </script>

  </body>
</html>