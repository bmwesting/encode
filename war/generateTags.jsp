<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>en-co.de</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="en-co.de">
    <meta name="author" content="Brandt Westing">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="img/favicon.ico">
    
    <script type="text/javascript" src="js/soyutils.js"></script>
    <script type="text/javascript" src="js/navbar.js"></script>
    <script type="text/javascript" src="js/navbar_util.js"></script>
  </head>

  <body>
    <script type="text/javascript">
        document.write(encode.navbar.navbar());
    </script>

    <div class="container">

      <h2>Batch Generate Tags</h2>
        <ul class="nav nav-list">
            <li class="divider"></li>
        </ul>
        <div class="row">
            <div class="span3">
                <div id="generate-div">
	                <form id="generate-form" action="/genTags" method="get">
					  <fieldset>
					    <label>Number of Tags to Generate</label>
					    <input type="text" id="generate-form-num" placeholder="Enter number from 1 to 50">
					    <button type="submit" class="btn btn-primary">Generate</button>
					  </fieldset>
					</form>
					
	                <p id="notification-text"></p>
                </div>
                <div id="instructions-div" style="display: none">
                <p>These tags will not become active until scanned.</p>
                
                <p>Upon scanning, you will be prompted to input a name and description and they will become active.</p>
                
                <button class="btn btn-primary" id="more-tags-button">Generate More Tags</button>
                </div>
            </div>
            <div class="span6">
                <div id="qr-grid">
                </div>
            </div>
        </div>

      <hr>

      <footer>
        <p>&copy; en-co.de 2013</p>
      </footer>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.9.1.min.js"></script>
    <script src="js/jquery.cookie.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/md5.js"></script>
    <script src="js/login.js"></script>
    
    <script>
    $(document).ready(function() {
        encode.navbar_util.initialize();
        
	     // create a new user account
	     $('#generate-form').submit(function() {

	         var num = $('#generate-form-num').val();
	                  
	         if(parseInt(num) < 1 || parseInt(num) > 50)
	       	 {
	       	   $('#notification-text').text("Please choose a value of 1 to 50.");
	       	   $('#notification-text').removeClass("text-success");
	           $('#notification-text').addClass("text-error");
	           $('#notification-text').fadeIn().delay(1500).fadeOut(1500);
	           
	           return false;
	       	 }
	         
	         var username = localStorage['username'];
	         var auth = localStorage['authenticationToken'];
	         
	         $.getJSON('/genTags', {user: username, auth: auth, num: num}, function(json){
	             if(json.status == "auth_fail"){
	            	 $('#notification-text').text("Invalid authentication token.");
	                 $('#notification-text').removeClass("text-success");
	                 $('#notification-text').addClass("text-error");
	                 $('#notification-text').fadeIn().delay(1500).fadeOut(1500);
	             }
	             else{
	                 // here show tags, new sidebar with instructions, and option to generate more
	                 var tags = json.tags;
	                 var count = 0;
                     QRPERROW = 3;
                     intCount = 0;

	                 $.each(tags, function (index, value) {
	                	 // create new row
	                	 intCount = parseInt(count/QRPERROW);
	                	 if((count % QRPERROW) == 0) {
	                		 $('#qr-grid').append("<div class=\"row\" id=\"grid-row"+intCount+"\"></div>");
	                	 }
	                	 var googleQRURL = "https://chart.googleapis.com/chart?cht=qr&chs=140x140&chld=M|0&chl=www.en-co.de/?t=" + 
	                     value;
	                	 
	                	 //alert($('#qr-grid').html());
	                	 
	                	 old = $('#grid-row'+intCount).html();
	                	 //alert('#grid-row'+intCount);
	                	 
	                	 content = old + "<div class=\"span2\"><img src=\""+googleQRURL+"\"\/></div>";
	                	 $('#grid-row'+intCount).html(content);
	                	 count++;

	                 });
	                 
	                 showTags();
	                 showInstructions();
	             }
	         });
	         return false;
	     });
	     
	     $('#more-tags-button').click(function() {
	    	 $('#instructions-div').hide();
	    	 $('#qr-grid').empty();
	    	 $('#generate-div').fadeIn();
	     });
    });
    
    function showTags(tags_json) {
    	
    }
    
    function showInstructions() {
    	$('#generate-div').hide();
    	$('#instructions-div').fadeIn();
    }
    </script>

  </body>
</html>