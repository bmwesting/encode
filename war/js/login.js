/*
 * Calls validation servlet to validate user/pass combo.
 * 
 * Returns 'ok', 'no_user', 'bad_pass'
 */
function onLoginSuccess(username){
	$('#id-dropdown').show();
	$('#id-login-form').hide();
	$('#id-login-selector').show();
	$('#id-login-selector-user').text(username);
}

function onLogout(){
	$('#id-dropdown').hide();
	$('#id-login-form').show();
	$('#id-login-selector').hide();
}