if (typeof encode == 'undefined') { var encode = {}; }
if (typeof encode.navbar_util == 'undefined') { encode.navbar_util = {}; }

encode.navbar_util.initialize = function() {
	// check local storage for auth token and authenticate
	var auth = localStorage['authenticationToken'];
	var username = localStorage['username'];
	if(auth != undefined && username != undefined){
		$.getJSON('validateToken', {user: username, token: auth}, function(json){
			$.each(json, function (index, value) {
				if(value == "token_err"){
					alert('token_err on token authentication');
				}
				if(value == "user_err"){
					onLogout();
					alert('user_err on token authentication');
				}
				else{
					onLoginSuccess(username);
				}
			});
		});
	}
	// show login form
	else{
		onLogout();
	}
	
	// validate credentials on login button press
	$('#sign_in_btn').click(function() {
		var username = $('#id-login-form').find('input[name="username"]').val();
		var password = CryptoJS.MD5($('#id-login-form').find('input[name="password"]').val()).toString(CryptoJS.enc.Hex);
		$.getJSON('validateCred', {user: username, pass: password}, function(json){
			$.each(json, function (index, value) {
				if(value == "user_err"){
					alert('user_err');
				}
				else if(value == "pass_err"){
					alert('pass_err');
				}
				else{
					onLoginSuccess(username);
					
					// now save authentication token to local storage
					localStorage['authenticationToken'] = value;
					localStorage['username'] = username;
				}
			});
		});	
	});
	
	// create a new user account
	$('#sign_up_btn').click(function() {
		window.location.replace("/newAccount.jsp");
		/*
		var username = $('#id-login-form').find('input[name="username"]').val();
		var password = CryptoJS.MD5($('#id-login-form').find('input[name="password"]').val()).toString(CryptoJS.enc.Hex);
		$.getJSON('newCred', {user: username, pass: password}, function(json){
			$.each(json, function (index, value) {
				if(value == "user_err"){
					alert('user_err');
				}
				else{
					onLoginSuccess(username);
					
					localStorage['authenticationToken'] = value;
					localStorage['username'] = username;
				}
			});
		});
		*/
	});
	
	// create a new user account
	$('#id-sign-out').click(function() {
		localStorage.removeItem('authenticationToken');
		localStorage.removeItem('username');
		onLogout();
	});
}