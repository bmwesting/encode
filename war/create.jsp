<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>en-co.de</title>
    
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js">
	</script>
  </head>

  <body>
    <h1>en-co.de a new tag</h1>
	
	<form name="create" method=post action="/newTag">
	<table>
	<tr>
		<td>Key:</td>
		<td><input id="randomField" name="key" type="text" value="Random"/></td>
	</tr>
	<tr>
		<td>Name:</td>
		<td><input type="text" name="name" value="Name"/></td>
	</tr>
	<tr>
		<td>Phone Number:</td>
		<td><input type="text" name="phone" value="5124842627"/></td>
	</tr>
	<tr>
		<td><input type="submit" value="Submit"/></td>
	</tr>
	</table>
    </form>
    <img id="qrImage" src="https://www.google.com/images/srpr/logo4w.png"/>
    
    <script>
	$(document).ready(function(){
	$.ajax({
		  url: 'uuid',
		  success: function(response) {
			  $("#randomField").val(response);
			  var googleQRURL = "https://chart.googleapis.com/chart?cht=qr&chs=150x150&chld=M|0&chl=www.en-co.de/?t=" + 
			  	response.replace(/\s/g, '');
			  $("#qrImage").attr("src", googleQRURL)
			  //https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=Hello%20world&choe=UTF-8
		  }
		});
	});
	</script>
  </body>
</html>