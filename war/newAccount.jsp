<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>en-co.de</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="en-co.de">
    <meta name="author" content="Brandt Westing">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="img/favicon.ico">
    
    <script type="text/javascript" src="js/soyutils.js"></script>
    <script type="text/javascript" src="js/navbar.js"></script>
    <script type="text/javascript" src="js/navbar_util.js"></script>
  </head>

  <body>
    <script type="text/javascript">
        document.write(encode.navbar.navbar());
    </script>

    <div class="container">
    
        <h2>New Account Sign-Up</h2>
        <p>
        Welcome to en-co.de! You can easily sign up for an en-co.de account here and starting tagging your possessions.
        </p>
        
        <div class="row">
            <ul class="nav nav-list">
                <li class="divider"></li>
            </ul>
        </div>
        <br>
        <form class="form-horizontal" id="submit-form" action="/newCred" method="get">
          <div class="control-group">
            <label class="control-label" for="submit-email">Email</label>
            <div class="controls">
              <input type="text" id="submit-email" placeholder="Email">
            </div>
          </div>
		  <div class="control-group">
		    <label class="control-label" for="submit-username">User Name</label>
		    <div class="controls">
		      <input type="text" id="submit-username" placeholder="UserName">
		    </div>
		  </div>
		  <div class="control-group">
		    <label class="control-label" for="submit-pass">Password</label>
		    <div class="controls">
		      <input type="password" id="submit-pass" placeholder="Password">
		    </div>
		  </div>
		  <div class="control-group">
		    <div class="controls">
		      <button type="submit" class="btn" id="submit-button">Sign Up</button>
		      <span class="help-inline"><p id="submit-notifier"></p></span>
		    </div>
		  </div>
		</form>

      <hr>

      <footer>
        <p>&copy; en-co.de 2013</p>
      </footer>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.9.1.min.js"></script>
    <script src="js/jquery.cookie.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/md5.js"></script>
    <script src="js/login.js"></script>
    
    <script>
    $(document).ready(function() {
        encode.navbar_util.initialize();
        
        $('#submit-notifier').hide();
        
        // create a new user account
        $('#submit-form').submit(function() {
            var username = $('#submit-username').val();
            var password = CryptoJS.MD5($('#submit-pass').val()).toString(CryptoJS.enc.Hex);
            $.getJSON('/newCred', {user: username, pass: password}, function(json){
                if(json.status == "user_err"){
                	$('#submit-notifier').text("Username already taken.");
                    $('#submit-notifier').removeClass("text-success");
                    $('#submit-notifier').addClass("text-error");
                    $('#submit-username').removeClass("error");
                    $('#submit-username').addClass("error");
                    $('#submit-notifier').fadeIn().delay(1500).fadeOut(1500);
                }
                else{
                	$('#submit-notifier').text("Success. You may now login.");
                    $('#submit-notifier').removeClass("text-error");
                    $('#submit-notifier').addClass("text-success");
                    $('#submit-username').removeClass("error");
                    $('#submit-notifier').fadeIn().delay(1500).fadeOut(1500);
                }
            });
            return false;
        });
        
        
    });
    </script>

  </body>
</html>