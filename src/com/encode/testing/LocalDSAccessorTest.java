package com.encode.testing;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;

import com.encode.dsaccessor.DSAccessor;
import com.encode.dsaccessor.DSAccessorImpl;
import com.encode.dsaccessor.QRMetaData;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

public class LocalDSAccessorTest {
	
	// maximum eventual consistency ensures writes commit but fail to apply - always
    private final LocalServiceTestHelper helper =
        new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig()
            .setDefaultHighRepJobPolicyUnappliedJobPercentage(100));
    
    // accessor abstracts the google datastore
    private final DSAccessor accessor = new DSAccessorImpl();

    @Before
    public void setUp() {
        helper.setUp();
    }

    @After
    public void tearDown() {
        helper.tearDown();
    }
    
    // retrieving a qr not in the datastore results in null
    @Test
    public void testEmptyQR()
    {
    	assertEquals(null, accessor.getQR("00000000"));
    }
    
    // ensure QR with equal metadata are equal
    @Test
    public void testEqualQR()
    {
    	QRMetaData qr = new QRMetaData();
    	qr.key = "00000000";
    	qr.name = "Bob's Drill";
    	accessor.putQR(qr); // put new qr into HRD
    	assertEquals(qr.key, accessor.getQR(qr.key).key); // compare the returned qr with the put qr
    }
    
    // ensure the accessor updates a previous qr
    @Test
    public void testDuplicateQR()
    {
    	QRMetaData qr1 = new QRMetaData();
    	qr1.key = "00000000";
    	qr1.name = "Bob's Drill";
    	QRMetaData qr2 = new QRMetaData();
    	qr2.key = "00000000";
    	qr2.name = "Dave's Drill";
    	accessor.putQR(qr2);
    	assertTrue(accessor.getQR(qr1.key).name.equals(qr2.name));
    }
    
    // adding a qr and reading returns correct qr
    @Test
    public void testAddMultipleQR()
    {
    	QRMetaData qr = new QRMetaData();
    	qr.key = "00000000";
    	qr.name = "Bob's Drill";
    	QRMetaData qr2 = new QRMetaData(qr);
    	qr2.key = "00000001";
    	
    	// put both into the datastore
    	accessor.putQR(qr);
    	accessor.putQR(qr2);
    	
    	// assert the key's are correct
    	assertEquals(qr.key, accessor.getQR(qr.key).key);
    	assertEquals(qr.name, accessor.getQR(qr.key).name);
    }

}
