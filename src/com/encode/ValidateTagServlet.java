package com.encode;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.encode.NewTagServlet.JsonResponseObj;
import com.encode.dsaccessor.DSAccessor;
import com.encode.dsaccessor.DSAccessorImpl;
import com.encode.dsaccessor.QRMetaData;
import com.encode.dsaccessor.UserMetaData;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class ValidateTagServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("application/json");
		
		JsonResponseObj obj = new JsonResponseObj();
		Gson gson = new Gson();
		
		String key = req.getParameter("key");
		String name = req.getParameter("name");
		String description = req.getParameter("description");
		
		DSAccessor accessor = new DSAccessorImpl();
		
		QRMetaData qr = accessor.getQR(key);
		qr.name = name;
		qr.description = description;
		qr.tracking = req.getParameter("tracking") != null ? true : false;
		qr.messaging = req.getParameter("messaging") != null ? true : false;
		qr.creationDate = System.currentTimeMillis();
		qr.valid = true;
		
		// ensured unique key
		accessor.putQR(qr);
		
		// get the users metadata and insert tag's key into user's keys
		UserMetaData uMD = accessor.getUser(qr.ownerID);
		if(uMD.tagKeys == null)
			uMD.tagKeys = new Vector<String>();
		uMD.tagKeys.add(key);
		accessor.putUser(uMD);
		
		obj.status = name;
		String json = gson.toJson(obj);
		resp.getWriter().print(json);
	}
	
	// have to use nested class for Gson reflection to work
	static class JsonResponseObj {
        @SuppressWarnings("unused")
		private String status = "ok";
        JsonResponseObj() {
            // no-args constructor
        }
    }
}
