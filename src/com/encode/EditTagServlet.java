package com.encode;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.encode.ScannedTagServlet.JsonResponseObj;
import com.encode.dsaccessor.DSAccessor;
import com.encode.dsaccessor.DSAccessorImpl;
import com.encode.dsaccessor.QRMetaData;
import com.encode.dsaccessor.UserMetaData;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class EditTagServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("application/json");
		
		JsonResponseObj obj = new JsonResponseObj();
		Gson gson = new Gson();
		
		String key = req.getParameter("key");
		String auth = req.getParameter("auth");
		String name = req.getParameter("name");
		String description = req.getParameter("description");
		
		DSAccessor accessor = new DSAccessorImpl();
		
		
		QRMetaData qr = accessor.getQR(key);
		UserMetaData uMD = accessor.getUser(qr.ownerID);
		if(!auth.equals(uMD.authToken)){
			obj.status = "invalid_auth";
			String json = gson.toJson(obj);
			resp.getWriter().print(json);
			return;
		}

		qr.name = name;
		qr.description = description;
		qr.tracking = req.getParameter("tracking") != null ? true : false;
		qr.messaging = req.getParameter("messaging") != null ? true : false;
		
		// ensured unique key
		accessor.putQR(qr);
		
		obj.status = "ok";
		String json = gson.toJson(obj);
		resp.getWriter().print(json);
	}
	
	// have to use nested class for Gson reflection to work
	static class JsonResponseObj {
        @SuppressWarnings("unused")
        private String status;
        
        JsonResponseObj() {
            // no-args constructor
        }
	}
}
