package com.encode;

import java.io.IOException;
import javax.servlet.http.*;

import com.encode.dsaccessor.DSAccessor;
import com.encode.dsaccessor.DSAccessorImpl;
import com.encode.util.UUIDGenerator;

@SuppressWarnings("serial")
public class UUIDServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		
		UUIDGenerator uuid = new UUIDGenerator();
		DSAccessor accessor = new DSAccessorImpl();
		
		// check for conflict with already generated tag
		String nextUUID = null;
		boolean unique = false;
		while(!unique)
		{
			nextUUID = uuid.generate(8);
			// if the UUID is not in the datastore, it is unique
			if (null == accessor.getQR(nextUUID))
				unique = true;
		}
		
		resp.getWriter().print(nextUUID);
	}
}
