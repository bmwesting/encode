package com.encode;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.encode.NewTagServlet.JsonResponseObj;
import com.encode.dsaccessor.DSAccessor;
import com.encode.dsaccessor.DSAccessorImpl;
import com.encode.dsaccessor.QRMetaData;
import com.encode.dsaccessor.UserMetaData;
import com.encode.util.UUIDGenerator;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class GenTagsServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("application/json");
		
		Gson gson = new Gson();
		
		String username = req.getParameter("user");
		String auth = req.getParameter("auth");
		int num = Integer.parseInt(req.getParameter("num"));
		
		DSAccessor accessor = new DSAccessorImpl();
		
		UserMetaData uMD = accessor.getUser(username);
		
		// check for authentication success, otherwise bail
		if(!uMD.authToken.equals(auth))
		{
			JsonResponseObj obj = new JsonResponseObj();
			obj.status = "auth_fail";
			String json = gson.toJson(obj);
			resp.getWriter().print(json);
			return;
		}
		
		Vector<String> newTags = new Vector<String>();
		UUIDGenerator gen = new UUIDGenerator();
		
		for(int i = 0; i < num; i++){
			String key = gen.generate(8);
			while(accessor.getQR(key) != null)
			{
				key = gen.generate(8);
			}
			
			QRMetaData qr = new QRMetaData();
			qr.key = key;
			qr.ownerID = username;
			qr.valid = false;
			newTags.add(key);
			accessor.putQR(qr);
		}
		
		JsonResponseObj obj = new JsonResponseObj();
		obj.tags = newTags;
		String json = gson.toJson(obj);
		resp.getWriter().print(json);
	}
	
	// have to use nested class for Gson reflection to work
	static class JsonResponseObj {
        @SuppressWarnings("unused")
        private String status;
        private Vector<String> tags;
        
        JsonResponseObj() {
            // no-args constructor
        }
	}
}
