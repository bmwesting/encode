package com.encode;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.encode.dsaccessor.DSAccessor;
import com.encode.dsaccessor.DSAccessorImpl;
import com.encode.dsaccessor.UserMetaData;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class ValidateTokenServlet extends HttpServlet {
	//private static final Logger log = Logger.getLogger(ValidateCredServlet.class.getName());
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("application/json");
		
		String username = req.getParameter("user");
		String token = req.getParameter("token");
				
		DSAccessor accessor = new DSAccessorImpl();
		JsonResponseObj obj = new JsonResponseObj();
		Gson gson = new Gson();
		
		UserMetaData md = accessor.getUser(username);

		// no such user
		if (null == md)
			obj.status = "user_err";
		else if(!token.equals(md.authToken))
			obj.status = "token_err";
		else
		{
			obj.status = "ok";
		}
		
		String json = gson.toJson(obj);
		resp.getWriter().print(json);
	}
	
	// have to use nested class for Gson reflection to work
	static class JsonResponseObj {
        @SuppressWarnings("unused")
		private String status = "ok";
        JsonResponseObj() {
            // no-args constructor
        }
    }
}
