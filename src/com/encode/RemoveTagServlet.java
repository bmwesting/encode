package com.encode;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.encode.dsaccessor.DSAccessor;
import com.encode.dsaccessor.DSAccessorImpl;

@SuppressWarnings("serial")
public class RemoveTagServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		String key = req.getParameter("key");
		
		DSAccessor accessor = new DSAccessorImpl();
		
		accessor.deleteQR(key);
	}
}
