package com.encode.dsaccessor;

import javax.persistence.Id;
import com.googlecode.objectify.annotation.Entity;

/** QRMetaData represents the data associated with a QR code 
*
* @author Brandt Westing
*/
@Entity
public class QRMetaData {

	/** Uniquely identifies the QR code */
	@Id public String key;
	
	/** Uniquely identifies the owner of this QR code */
	public String ownerID;
	
	/** User supplied name of the object */
	public String name;
	
	/** User supplied description of the object */
	public String description;
	
	/** Date when the object was last scanned/retrieved in ms from current era */
	public long tScanned;
	
	/** Location coordinates provided by user's browser when scanning a tag */
	public String lon;
	public String lat;
	public String alt;
	
	/** True if position tracking is enabled for this QR code
	 * 
	 */
	public boolean tracking;
	
	/** True if text message alerts are enabled for this QR code
	 * 
	 */
	public boolean messaging;
	
	/** date the tag was created
	 * 
	 */
	public long creationDate;
	
	public boolean valid;
	
	public QRMetaData(QRMetaData q)
	{
		key = q.key;
		ownerID = q.ownerID;
		name = q.name;
		description = q.description;
		tScanned = q.tScanned;
		lat = q.lat;
		lon = q.lon;
		alt = q.alt;
		tracking = q.tracking;
		messaging = q.messaging;
	}

	public QRMetaData() {
		// TODO Auto-generated constructor stub
	}
}
