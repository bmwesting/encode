package com.encode.dsaccessor;

import java.util.Vector;

/** DSAccessor abstracts the Google High Replication Datastore 
*
* @author Brandt Westing
*/
public interface DSAccessor {
	
	/** getQR retrieves a QRMetaData object from the HRD
	 * 
	 * @param key			A string of length (8) that uniquely identifies the QR object
	 * @return				null if no qr found, else a QRMetaData object associated with key
	 */
	public QRMetaData getQR(String key);
	
	/** retrieves qr codes given a username
	 * 
	 * @param username
	 * @return				Vector of QRMetaData objects
	 */
	public Vector<QRMetaData> getQRTagsByUsername(String username);
	
	/** putQR put's a QRMetaData object into the HRD
	 * 
	 * @param qr			A QRMetaData object
	 */
	public void putQR(QRMetaData qr);
	
	/** putUser put's a UserMetaData object into the HRD
	 * 
	 * @param user			A UserMetaData object
	 */
	public void putUser(UserMetaData user);
	
	/** getUser retrieves a UserMetaData object from the HRD
	 * 
	 * @param key			A string that uniquely identifies the User object (email)
	 * @return				null if no qr found, else a UserMetaData object associated with key
	 */
	public UserMetaData getUser(String key);
	
	/** removed QR Meta Data from datastore and keylist of user
	 * 
	 * @param key			Key for QR MetaDataType
	 */
	public void deleteQR(String key);

}
