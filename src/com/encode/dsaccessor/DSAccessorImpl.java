package com.encode.dsaccessor;

//import com.googlecode.objectify.Objectify;
//import com.googlecode.objectify.ObjectifyService;

import static com.encode.dsaccessor.OfyService.ofy;

import java.util.Vector;

import com.googlecode.objectify.NotFoundException;

/** DSAccessor abstracts the Google High Replication Datastore 
*
* @author Brandt Westing
*/
public class DSAccessorImpl implements DSAccessor{

	@Override
	public QRMetaData getQR(String key) {
		QRMetaData qr;
		try {
			qr = ofy().get(QRMetaData.class, key);
		} catch (NotFoundException ex) {
			qr = null;
		}
		return qr;
	}
	
	@Override
	public Vector<QRMetaData> getQRTagsByUsername(String username) {
		
		Vector<QRMetaData> result = new Vector<QRMetaData>();
		
		UserMetaData uMD = getUser(username);
		
		if(uMD.tagKeys == null)
			return null;
		
		for(String key : uMD.tagKeys)
		{
			result.add(getQR(key));
		}
		
		return result;
	}

	@Override
	public void putQR(QRMetaData qr) {
		ofy().put(qr);
	}
	
	@Override
	public UserMetaData getUser(String key){
		UserMetaData user;
		try {
			user = ofy().get(UserMetaData.class, key);
		} catch (NotFoundException ex) {
			user = null;
		}
		return user;
	}
	
	@Override
	public void putUser(UserMetaData user) {
		ofy().put(user);
	}
	
	@Override
	public void deleteQR(String key) {
		QRMetaData qMD = getQR(key);
		UserMetaData uMD = getUser(qMD.ownerID);
		
		// remove key from owner's key list
		int index = 0;
		for(String uMDKey : uMD.tagKeys)
		{
			if (uMDKey.equals(key))
			{
				uMD.tagKeys.remove(index);
				break;
			}
			index++;
		}
		putUser(uMD);
		
		// remove key from database
		ofy().delete(QRMetaData.class, key);
	}

}
