package com.encode.dsaccessor;

import java.util.Vector;

import javax.persistence.Id;
import com.googlecode.objectify.annotation.Entity;

@Entity
public class UserMetaData {
	
	/** Uniquely identifies the user */
	@Id public String username;
	
	/** Users password stored as MD5 hash (generated on client) */
	public String password;
	
	/** Authentication token used for logging user in without manual input */
	public String authToken;
	
	/** Phone number used for auto-text feature
	 *  Format Specification: 5555555555 (Example format for user input (555) 555-5555)
	 */
	public String phone;
	
	/** Email address of user
	 * 
	 */
	public String email;
	
	public String name;
	
	public String addStreet;
	
	public String addCity;
	
	public String addState;
	
	public String addZip;
	
	public boolean shareName;
	
	public boolean sharePhone;
	
	public boolean shareEmail;
	
	public boolean shareAdd;
	
	/** Vector of key ID's
	 * 
	 */
	public Vector<String> tagKeys;

}
