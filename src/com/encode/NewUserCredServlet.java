package com.encode;

import java.io.IOException;
import java.util.Vector;
//import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import com.encode.NewUserCredServlet.JsonResponseObj;
import com.encode.dsaccessor.DSAccessor;
import com.encode.dsaccessor.DSAccessorImpl;
import com.encode.dsaccessor.UserMetaData;
import com.encode.util.UUIDGenerator;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class NewUserCredServlet extends HttpServlet {
	//private static final Logger log = Logger.getLogger(ValidateCredServlet.class.getName());
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("application/json");
		
		String username = req.getParameter("user");
		String password = req.getParameter("pass");
				
		DSAccessor accessor = new DSAccessorImpl();
		JsonResponseObj obj = new JsonResponseObj();
		Gson gson = new Gson();
		
		UserMetaData md = accessor.getUser(username);

		// no such user
		if (null == md)
		{
			
			// generate an authentication token
			UUIDGenerator gen = new UUIDGenerator();
			String salt = gen.generate(16); // generate a random salt hash
			String token = gen.generate(16, salt+username); // generate token by hashing the salt combined with the username
			
			// add to datastore
			md = new UserMetaData();
			md.username = username;
			md.password = password;
			md.authToken = token;
			md.tagKeys = new Vector<String>();
			accessor.putUser(md);
			
			// return the token to the client
			obj.status = token;
		}
		else
			obj.status = "user_err";
		
		String json = gson.toJson(obj);
		resp.getWriter().print(json);
	}
	
	// have to use nested class for Gson reflection to work
	static class JsonResponseObj {
        @SuppressWarnings("unused")
		private String status = "ok";
        JsonResponseObj() {
            // no-args constructor
        }
    }
}

