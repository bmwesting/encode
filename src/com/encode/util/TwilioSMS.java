package com.encode.util;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.SmsFactory;
import com.twilio.sdk.resource.instance.Sms;
import com.twilio.sdk.resource.list.SmsList;
import java.util.HashMap;
import java.util.Map;

/** SMSText sends a text message to a given number using Twilio.
*
* @author Brandt Westing
*/
public class TwilioSMS {
	
	public static final String ACCOUNT_SID = "AC6079e734725d2b5918df1a0933e2a14c";
	public static final String AUTH_TOKEN = "f3b00ae79d84c6a85656b8462e73863e";

	public void sendText(String number, String txt) throws TwilioRestException
	{
		TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
		 
	    // Build a filter for the SmsList
	    Map<String, String> params = new HashMap<String, String>();
	    params.put("Body", "Testing twilio for en-co.de.");
	    params.put("To", "+15124842627");
	    params.put("From", "+15005550006");
	     
	    SmsFactory messageFactory = client.getAccount().getSmsFactory();
	    Sms message = messageFactory.create(params);
	    System.out.println(message.getSid());
	}
}
