package com.encode.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/** UUIDGenerator generates a random sequence of alpha-numeric characters with given length
*
* @author Brandt Westing
*/
public class UUIDGenerator {

	public String generate(int length)
	{	
		// generate a random number in string form
		Random randomGenerator = new Random();
		String random = Integer.toString(randomGenerator.nextInt());
		
		return generate(length, random);
	}
	
	// generates and MD5 hash substring given a seed string
	public String generate(int length, String seed)
	{
		MessageDigest digest;
		
		try {
			digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
		
		digest.update(seed.getBytes(), 0, seed.length());
		String md5 = new BigInteger(1, digest.digest()).toString(16);
		
		return md5.substring(0, length);
	}
}
