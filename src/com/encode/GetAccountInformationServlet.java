package com.encode;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.encode.dsaccessor.DSAccessor;
import com.encode.dsaccessor.DSAccessorImpl;
import com.encode.dsaccessor.UserMetaData;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class GetAccountInformationServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("application/json");
		
		JsonResponseObj obj = new JsonResponseObj();
		Gson gson = new Gson();
		
		String username = req.getParameter("username");
		
		// get tags by username
		DSAccessor accessor = new DSAccessorImpl();
		UserMetaData uMD = accessor.getUser(username);
		
		obj.name = uMD.name;
		obj.email = uMD.email;
		obj.phone = uMD.phone;
		obj.addStreet = uMD.addStreet;
		obj.addCity = uMD.addCity;
		obj.addState = uMD.addState;
		obj.addZip = uMD.addZip;
		obj.allowName = uMD.shareName;
		obj.allowPhone = uMD.sharePhone;
		obj.allowAdd = uMD.shareAdd;
		obj.allowEmail = uMD.shareEmail;
		
		String json = gson.toJson(obj);
		resp.getWriter().print(json);
	}
	
	// have to use nested class for Gson reflection to work
	static class JsonResponseObj {
        @SuppressWarnings("unused")
		private String name;
        private String email;
        private String phone;
        private String addStreet;
        private String addCity;
        private String addState;
        private String addZip;
        private boolean allowName;
        private boolean allowPhone;
        private boolean allowEmail;
        private boolean allowAdd;
        
        JsonResponseObj() {
            // no-args constructor
        }
	}
}
