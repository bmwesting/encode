package com.encode;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import java.util.logging.Logger;

import com.encode.dsaccessor.DSAccessor;
import com.encode.dsaccessor.DSAccessorImpl;
import com.encode.dsaccessor.UserMetaData;
import com.encode.util.UUIDGenerator;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class ValidateCredServlet extends HttpServlet {
	//private static final Logger log = Logger.getLogger(ValidateCredServlet.class.getName());
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("application/json");
		
		String username = req.getParameter("user");
		String password = req.getParameter("pass");
				
		DSAccessor accessor = new DSAccessorImpl();
		JsonResponseObj obj = new JsonResponseObj();
		Gson gson = new Gson();
		
		UserMetaData md = accessor.getUser(username);

		// no such user
		if (null == md)
			obj.status = "user_err";
		else if(!password.equals(md.password))
			obj.status = "pass_err";
		else
		{
			// generate an authentication token
			UUIDGenerator gen = new UUIDGenerator();
			String salt = gen.generate(16); // generate a random salt hash
			String token = gen.generate(16, salt+username); // generate token by hashing the salt combined with the username
			
			// update the datastore with new token
			md.authToken = token;
			accessor.putUser(md);
			
			// return the token to the client
			obj.status = token;
		}
		
		String json = gson.toJson(obj);
		resp.getWriter().print(json);
	}

	// have to use nested class for Gson reflection to work
	static class JsonResponseObj {
        @SuppressWarnings("unused")
		private String status = "ok";
        JsonResponseObj() {
            // no-args constructor
        }
    }
}

