package com.encode;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.encode.RetrieveTagServlet.JsonResponseObj;
import com.encode.dsaccessor.DSAccessor;
import com.encode.dsaccessor.DSAccessorImpl;
import com.encode.dsaccessor.QRMetaData;
import com.encode.dsaccessor.UserMetaData;
import com.google.gson.Gson;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.SmsFactory;
import com.twilio.sdk.resource.instance.Sms;

@SuppressWarnings("serial")
public class ScannedTagServlet extends HttpServlet {
	
	public static final String ACCOUNT_SID = "AC6079e734725d2b5918df1a0933e2a14c";
	public static final String AUTH_TOKEN = "f3b00ae79d84c6a85656b8462e73863e";
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("application/json");
		
		JsonResponseObj obj = new JsonResponseObj();
		Gson gson = new Gson();
		
		String key = req.getParameter("key");
		//String auth = req.getParameter("auth");
		
		DSAccessor accessor = new DSAccessorImpl();
		QRMetaData qMD = accessor.getQR(key);
		
		// tag not validated, interface should prompt user to validate
		if(qMD.valid == false)
		{
			obj.status = "invalid";

			String json = gson.toJson(obj);
			resp.getWriter().print(json);
			
			return;
		}
		
		UserMetaData uMD = accessor.getUser(qMD.ownerID);
		
		// update scan time
		qMD.tScanned = System.currentTimeMillis();
		accessor.putQR(qMD);
		
		// send text message if appropriate
		if(qMD.messaging)
		{
			// send text message
			TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
			  
		    // Build a filter for the SmsList
		    Map<String, String> params = new HashMap<String, String>();
		    params.put("Body", "en-co.de tag " + qMD.key + " has been scanned. Visit en-co.de to see updates.");
		    params.put("To", uMD.phone);
		    params.put("From", "(512) 605-1623");
		      
		    SmsFactory messageFactory = client.getAccount().getSmsFactory();
		    Sms message = null;
			try {
				message = messageFactory.create(params);
			} catch (TwilioRestException e) {
			
				System.out.println("Error creating text message.");
			}
			
			//if(message != null)
				//System.out.println(message.getSid());
			
			//send email message
			Properties props = new Properties();
	        Session session = Session.getDefaultInstance(props, null);

	        String msgBody = "QR Tag " + qMD.key + " was just scanned. You can see updates at en-co.de/viewTags.jsp\n\n Thank you,\nen-co.de Staff";

	        try {
	            Message msg = new MimeMessage(session);
	            msg.setFrom(new InternetAddress("bmwesting@gmail.com", "En-co.de Admin"));
	            msg.addRecipient(Message.RecipientType.TO,
	                             new InternetAddress(uMD.email));
	            msg.setSubject("Your en-co.de tag was scanned. TagID: " + qMD.key);
	            msg.setText(msgBody);
	            Transport.send(msg);
	    
	        } catch (AddressException e) {
	            // ...
	        } catch (MessagingException e) {
	            // ...
	        }
		}
		
		// send email if appropriate
		
		obj.name = qMD.name;
		obj.description = qMD.description;
		if(uMD.shareName)
			obj.ownerName = uMD.name;
		if(uMD.shareEmail)
			obj.email = uMD.email;
		if(uMD.sharePhone)
			obj.phone = uMD.phone;
		if(uMD.shareAdd)
		{
			obj.addStreet = uMD.addStreet;
			obj.addCity = uMD.addCity;
			obj.addState = uMD.addState;
			obj.addZip = uMD.addZip;
		}
		if(qMD.messaging)
			obj.messaging = true;
		if(qMD.tracking)
			obj.tracking = true;
		
		obj.status = "ok";
		
		String json = gson.toJson(obj);
		resp.getWriter().print(json);
	}
	
	// have to use nested class for Gson reflection to work
	static class JsonResponseObj {
        @SuppressWarnings("unused")
        private String status;
        private String name;
        private String description;
		private String ownerName;
        private String email;
        private String phone;
        private String addStreet;
        private String addCity;
        private String addState;
        private String addZip;
        private boolean messaging;
        private boolean tracking;
        
        JsonResponseObj() {
            // no-args constructor
        }
	}
}
