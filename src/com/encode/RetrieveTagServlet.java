package com.encode;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.encode.dsaccessor.DSAccessor;
import com.encode.dsaccessor.DSAccessorImpl;
import com.encode.dsaccessor.QRMetaData;
import com.encode.dsaccessor.UserMetaData;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class RetrieveTagServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("application/json");
		
		JsonResponseObj obj = new JsonResponseObj();
		Gson gson = new Gson();
		
		String key = req.getParameter("key");
		
		DSAccessor accessor = new DSAccessorImpl();
		QRMetaData qMD = accessor.getQR(key);
		UserMetaData uMD = accessor.getUser(qMD.ownerID);
		
		// update scan time
		//qMD.tScanned = System.currentTimeMillis();
		//accessor.putQR(qMD);
		
		// send text message if appropriate
		
		// send email if appropriate
		
		obj.name = qMD.name;
		obj.description = qMD.description;
		if(uMD.shareName)
			obj.ownerName = uMD.name;
		if(uMD.shareEmail)
			obj.email = uMD.email;
		if(uMD.sharePhone)
			obj.phone = uMD.phone;
		if(uMD.shareAdd)
		{
			obj.addStreet = uMD.addStreet;
			obj.addCity = uMD.addCity;
			obj.addState = uMD.addState;
			obj.addZip = uMD.addZip;
		}
		if(qMD.messaging)
			obj.messaging = true;
		if(qMD.tracking)
			obj.tracking = true;
		
		String json = gson.toJson(obj);
		resp.getWriter().print(json);
	}
	
	// have to use nested class for Gson reflection to work
	static class JsonResponseObj {
        @SuppressWarnings("unused")
        private String name;
        private String description;
		private String ownerName;
        private String email;
        private String phone;
        private String addStreet;
        private String addCity;
        private String addState;
        private String addZip;
        private boolean messaging;
        private boolean tracking;
        
        JsonResponseObj() {
            // no-args constructor
        }
	}
}
