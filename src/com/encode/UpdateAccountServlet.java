package com.encode;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.encode.dsaccessor.DSAccessor;
import com.encode.dsaccessor.DSAccessorImpl;
import com.encode.dsaccessor.UserMetaData;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class UpdateAccountServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("application/json");
		
		JsonResponseObj obj = new JsonResponseObj();
		Gson gson = new Gson();
		
		String username = req.getParameter("username");
		String name = req.getParameter("name");
		String email = req.getParameter("email");
		String phone = req.getParameter("phone");
		String addStreet = req.getParameter("addStreet");
		String addCity = req.getParameter("addCity");
		String addState = req.getParameter("addState");
		String addZip = req.getParameter("addZip");
		
		DSAccessor accessor = new DSAccessorImpl();
		UserMetaData uMD = accessor.getUser(username);
		
		uMD.name = name;
		uMD.email = email;
		uMD.phone = phone;
		uMD.addStreet = addStreet;
		uMD.addCity = addCity;
		uMD.addState = addState;
		uMD.addZip = addZip;
		uMD.shareName = req.getParameter("allowName") != null ? true : false;
		uMD.shareEmail = req.getParameter("allowEmail") != null ? true : false;
		uMD.shareAdd = req.getParameter("allowAdd") != null ? true : false;
		uMD.sharePhone = req.getParameter("allowPhone") != null ? true : false;
		
		accessor.putUser(uMD);
		
		obj.status = name;
		String json = gson.toJson(obj);
		resp.getWriter().print(json);
	}
	
	// have to use nested class for Gson reflection to work
	static class JsonResponseObj {
        @SuppressWarnings("unused")
		private String status = "ok";
        JsonResponseObj() {
            // no-args constructor
        }
    }
}
