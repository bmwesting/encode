package com.encode;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.encode.NewTagServlet.JsonResponseObj;
import com.encode.dsaccessor.DSAccessor;
import com.encode.dsaccessor.DSAccessorImpl;
import com.encode.dsaccessor.QRMetaData;
import com.encode.dsaccessor.UserMetaData;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class RetrieveTagsServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("application/json");
		
		//JsonResponseObj obj = new JsonResponseObj();
		Gson gson = new Gson();
		
		String username = req.getParameter("user");
		String auth = req.getParameter("auth");
		
		DSAccessor accessor = new DSAccessorImpl();
		UserMetaData uMD = accessor.getUser(username);
		
		// check for authentication success, otherwise bail
		if(!uMD.authToken.equals(auth))
		{
			JsonResponseObj obj = new JsonResponseObj();
			obj.status = "auth_fail";
			String json = gson.toJson(obj);
			resp.getWriter().print(json);
			return;
		}
		
		// get tags by username
		Vector<QRMetaData> tags = accessor.getQRTagsByUsername(username);
		
		// convert tags to json
		String json = gson.toJson(tags);
		resp.getWriter().print(json);
	}
	
	// have to use nested class for Gson reflection to work
	static class JsonResponseObj {
        @SuppressWarnings("unused")
        private String status;
        
        JsonResponseObj() {
            // no-args constructor
        }
	}
}
