package com.encode;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.encode.RetrieveTagServlet.JsonResponseObj;
import com.encode.dsaccessor.DSAccessor;
import com.encode.dsaccessor.DSAccessorImpl;
import com.encode.dsaccessor.QRMetaData;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class UpdateLocationServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("application/json");
		
		JsonResponseObj obj = new JsonResponseObj();
		Gson gson = new Gson();
		
		String lat = req.getParameter("lat");
		String lon = req.getParameter("lon");
		String alt = req.getParameter("alt");
		String key = req.getParameter("key");
		
		DSAccessor accessor = new DSAccessorImpl();
		QRMetaData qMD = accessor.getQR(key);
		qMD.lat = lat;
		qMD.lon = lon;
		qMD.alt = alt;
		accessor.putQR(qMD);
		
		obj.status = "ok";
		String json = gson.toJson(obj);
		resp.getWriter().print(json);
		
	}
	
	// have to use nested class for Gson reflection to work
	static class JsonResponseObj {
        @SuppressWarnings("unused")
        public String status;
        
        JsonResponseObj() {
            // no-args constructor
        }
	}
}
